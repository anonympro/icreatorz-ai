#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <boost/algorithm/string.hpp>
using namespace std;
void outputtable(){

}
class CSVReader{
	string fileName;
	string delimeter;
 
public:
	CSVReader(string filename, string delm = ",") :
			fileName(filename), delimeter(delm)
	{ }
 
	vector<vector<string> > getData();
};
 
vector<vector<string> > CSVReader::getData(){
	ifstream file(fileName);
 
	vector<vector<string> > dataList;
 
	string line = "";
	while (getline(file, line)){
		vector<string> vec;
		boost::algorithm::split(vec, line, boost::is_any_of(delimeter));
		dataList.push_back(vec);
	}
	file.close();
 
	return dataList;
}
int main(){
	int counter=0;
	CSVReader reader("ncov_confirmed.csv");
	vector<vector<string> > confirmed = reader.getData();
	reader = CSVReader("ncov_deaths.csv");
	vector<vector<string> > deaths = reader.getData();
	reader = CSVReader("ncov_recovered.csv");
	vector<vector<string> > recovered = reader.getData();
 	vector<vector<string> > output;
	output.resize(2593); 
    for (auto &inner : output){
        inner.resize(6);
    }
	output[0][0]="Date";
	output[0][1]="Province/State";
	output[0][2]="Country/Region";
	output[0][3]="Confirmed";
	output[0][4]="Deaths";
	output[0][5]="Recovered";
	//vector[1][0]-->[2592][0] Process date//
	for(int i = 4; i<40; i++){
		for(int k = 0; k<72; k++){
			counter++;
			output[counter][0]=confirmed[0][i];
		}
	}
	counter=0;	
	//vector[1][1]-->[2592][1] Process Province/State//
	for(int i = 0; i<36; i++){
		for(int j = 1; j<73; j++){
			counter++;
			output[counter][1]=confirmed[j][0];
			//cout << counter << '\t' << output[counter][1] << endl;
		}
	}
	counter=0;
	//vector[1][2]-->[2592][2]
	for(int i = 0; i<36; i++){
		for(int j = 1; j<73; j++){
			counter++;
			output[counter][2]=confirmed[j][1];
		}
	}
	counter=0;
	//vector[1][3]-->[2592][3] <confirmed>
	for(int i = 4; i<40; i++){
		for(int j = 1; j<73; j++){
			counter++;
			output[counter][3]=confirmed[j][i];
		}
	}
	counter=0;
	//vector[1][4]-->[2592][4] <deaths>
	for(int i = 4; i<40; i++){
		for(int j = 1; j<73; j++){
			counter++;
			output[counter][4]=deaths[j][i];
		}
	}
	/*
	for(int i = 0; i<2593; i++){
		for(int j = 0; j<5; j++){
			cout << output[i][j] << ", ";
		}
		cout << endl;
	}
	*/
	counter=0;
	for(int i = 4; i<40; i++){
		for(int j = 1; j<73; j++){
			counter++;
			output[counter][5]=deaths[j][i];
		}
	}
	/*
	for(int i = 0; i<2593; i++){
		for(int j = 0; j<6; j++){
			cout << output[i][j] << ;
		}
		cout << endl;
	}
	*/
	ofstream myfile;
	myfile.open("output.txt");
	for(int i = 0; i<2593; i++){
		for(int j = 0; j<6; j++){
			myfile << output[i][j] << "," << "\t";
		}
		myfile << endl;
	}
	myfile.close();

}